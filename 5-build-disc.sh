#!/bin/ash
sdcard=$1
droid_rootfs="$(pwd)/rootfs/alpine/3.14.2_armv7"

sudo umount ${droid_rootfs}/proc
sudo umount ${droid_rootfs}/dev

sudo cp archives/firmware/ti-connectivity/TIInit_10.6.15.bts ${droid_rootfs}/lib/firmware/ti-connectivity/

set -u
mount | grep /dev/$sdcard | cut -d ' ' -f 3 | xargs umount
sudo parted /dev/${sdcard} mklabel gpt --script
sudo parted /dev/${sdcard} mkpart primary 0% 2% --script
sudo parted /dev/${sdcard} set 1 boot on --script
sudo parted /dev/${sdcard} mkpart primary 2% 95% --script
sudo parted /dev/${sdcard} mkpart primary 95% 100% --script

sudo partprobe /dev/${sdcard}
sudo mkfs.vfat /dev/${sdcard}1 -n BOOT
sudo mkfs.ext4 /dev/${sdcard}2
sudo mkfs.vfat /dev/${sdcard}3 -n USER_DATA
set +u

sleep 30

sudo mount /dev/${sdcard}1 mnt

sudo rsync --chown=root:root configs/boot.cfg linux/linux-stable/arch/arm/boot/zImage linux/linux-stable/arch/arm/boot/dts/omap4-droid4-xt894.dtb mnt/
sed -i 's|/boot/||g" /mnt/boot.cfg

sudo umount mnt

sudo mount /dev/${sdcard}2 mnt

sudo rsync --delete -av ${droid_rootfs}/ mnt/
cd modules/lib
sudo cp -a modules/ ${droid_rootfs}/lib/
cd ../..

sudo umount mnt
sudo mount /dev/${sdcard}3 mnt

sudo mkdir -p mnt/droid4/modem/dynamic_data
sudo mkdir -p mnt/droid4/modem/logs
sudo mv ${droid_rootfs}/tmp/modem.db mnt/droid4/modem/dynamic_data/

sudo umount mnt
echo "Disc finished imaging"
