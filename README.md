# Issues:
battery script is only semi functional
i3 config could be so much cooler
awesome config needs to be fleshed out
If udev-settle/postmount is enabled, the system boots quickly, if disabled it hangs booting. If enabled the CPU usage gets pinned around 60% by udev at what appears to be random times (idling much lower without it)
tlp may or may not help battery life, undetermined.

# TODO:
Remove tlp, eats too much CPU
Remove wpa_supplicant, ensure udev and udev-trigger aren't started at boot
(allow network manager to handle the networking instead of wpa_supplicant allows networking, and prevents udev from starting and chewing on the CPU)
Extend i3config
Create linux-droid apkbuild for easier kernel upgrades

# Attribution:
Tony Lindgren (tmlind) has done the majority of this work, and what you find here is built on top of that work. The kernel work is largely his, the build scripts are largely based on Omerelle's work. 
Configurations are based on a combination of Maemo Leste and PostmarketOS configurations.
If I've missed anything let me know, I just wanted to make it easier to simply build Alpine Droids

## TODO
add driver patch apks

# Building & Imaging Instructions:
Please be careful. If you are not careful you could wipe the wrong disc. Check your install device with fdisk -l before proceeding. Also note I have not tested step 2 yep, I've only done that manually. Testing is planned, but I suggest you manually issue each command at this time. 

## Layout Project:
./0-layout.sh

## Fetch Deps:
./1-fetch.sh

## Flash Device:
You probably should do this manually, but otherwise,
./2-fastboot-flash.sh

## Build Kernel:
If you want, the xt894-config can be copied to $(pwd)/linux/.config, it contains tweaks needed for netfilter and wireguard to work.
docker run -it -v $(pwd)/linux:/linux -v $(pwd)/modules:/modules kernel-compile

In the container run
build-kernel.sh then run build-mods.sh
Once finished exit the container

## Make rootfs
./3-mkrootfs.sh

## Chroot and Build:
(pick one desktop name, provide a username)
./4-chroot-build.sh (xfce, i3, awesome, tilde) (username)
(chroot build will build the base system, user/root passwords will be defaulted to droidxt894)

(if this doesn't suite you run ./enter-chroot.sh and proceed manually. Chroot build scripts are located in /usr/local/bin/)

(tilde option will convert the Droid4 into an Alpine based Tilde server (public unix server))

## Build disc:
./5-build-disc.sh sdX
Official documents on the droid4 say it can't use an sd card bigger than 32gb, but this is a limitation of Android and not applicable here.
I've tested thus far with a 32GB, 64GB, and have plans to test a 128GB shortly.

## Finished
Place the sd card in the droid4, and power it on
kexecboot should load and show an option labeled alpine
booting will cause the phone to vibrant, it takes about a minute to boot.

root/$username passwords will both be set to droidxt894 after build. Please change them!

## Keymapping:
If not using an X server:
Pressing OK will change the following keys thusly:
[1-‘Escape’, 2-‘;’, 3-‘:’, 4-‘|’, 5-‘`’, 6-‘~’, 7-‘{‘, 8-‘}’, 9-‘[‘, 0-‘]’, /-‘\’]

With an X server the key mapping becomes a little odd.
[shift-'ctrl', caps lock-'shift', sym-'alt']
if you have altalt installed (which you should) you access these characters from X11 via keycombos
tapping alt (sym)
sym + sym + j = |
sym + sym + . = :
sym + sym + , = ;
sym + sym + 0 = ]
sym + sym + 9 = [
sym + sym + 1 = esc
sym + sym + tab = ~
sym + sym + hold tab + 9 = {
sym + sym + hold tab + 0 = }
(caps lock sometimes works in lieu of tab, using tab toggles caps lock)'

## Install altalt:
git clone https://gitlab.com/Durrendal/Esper.git
cd Esper
make compile-lua
sudo make install-lua
sudo esper repo/altalt-droid.esper

## Disable Touchscreen
I hate them, you can turn it off with
xinput disable 6 (should be id=6, check with xinput --list)

## Battery Stretching
WWAN, Audio, and USB eat a bunch of battery if PM isn't enabled
You can blacklist phy_mapphone_mdm6600 to disable these
Is that very phone-y? No, but battery
