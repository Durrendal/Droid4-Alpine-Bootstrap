# What?
Potentially helpful scripts/utilities made specifically for the droid

# Battery
Report an estimated battery percentage, it's accurate enough, plug the droid in when it gets close to 10%, around that low it's not very accurate.

# Wifi
## str.sh
Shows the currently connected wifi singal strength as a simple percentage. I personally use this in my tmux.conf

# Scripts

I need to organize this better..

## usb_configs.sh
This sets up and enables the usb interface on the droid

## dscreen.sh
An X display changer. I use the droid mobile, connected to a lapdock (it's a cool accessory made for this line of droids that turns it into a laptop), and to display on an actual display. This isn't really necessary if you use something like XFCE4 to manage your displays
