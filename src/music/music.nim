import std/[os, osproc, strutils]
#nim c -d:release music.nim

proc getStatus(): string =
  let
    status = execCmdEx("mocp -Q '%state'")
  return status.output.replace("\n", "")

proc checkRunning(state: string): bool =
    if "FATAL_ERROR" in state:
      return false
    else:
      case state:
        of "PLAY", "PAUSE", "STOP":
          return true

proc getSong(): string =
  let
    song = execCmdEx("mocp -Q '%song'")
  return song.output.replace("\n", "")

proc getAlbum(): string =
  let
    album = execCmdEx("mocp -Q '%title'")
  return album.output.replace("\n", "")

proc getArtist(): string =
  let
    artist = execCmdEx("mocp -Q '%artist'")
  return artist.output.replace("\n", "")

proc main(): void =
  let
    status = getStatus()
    running = check_running(status)

  if running:
    var
      symbol = ""
      song = getSong()
      color = ""

    case status:
      of "PLAY":
        symbol = "> "
        color = "#9ab8d7"
      of "PAUSE":
        symbol = "|| "
        color = "#705050"
      of "STOP":
        symbol = "[]"
        color = "#dcdcdc"

    if paramCount() != 1:
      echo symbol & song
    elif paramStr(1) == "-i3":
      echo symbol & song
      echo song
      echo color

main()
