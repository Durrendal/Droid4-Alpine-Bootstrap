## What?

Just some battery scripts. They functionally do the same thing, but I suggest the nim one as it's smaller and a little more functional.

## battery.nim

You can use battery without args and get the current charge percentage as a string:
```
./battery
63.0%
```

Or pass it -i3 as an argument and it'll ouput long, short, and color code info for i3blocks
```
./battery -i3
Batt: 63.0%
63.0%
#FFF600
```

## Battery.go

This just returns the percentage as a string, but maybe you don't like nim?
