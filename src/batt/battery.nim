import std/[os, math, strutils, times]
#nim c -d:release battery.nim

#Expand proc & to concat float & strings
proc `&` (f: float, s:string): string = $f & s
proc `&` (s: string, f: float): string = s & $f

#calculate rough battery percentage from Droid4 voltage_now file
proc batt(): float =
  let
    sys = "/sys/class/power_supply/battery/"
    max = readFile(sys & "voltage_max_design").strip().parseInt
    min = readFile(sys & "voltage_min_design").strip().parseInt
    now = readFile(sys & "voltage_now").strip().parseInt()
    perc = (100 - ((max - now) * 100) / (max - min))
  return round(perc)

proc log(perc: float) =
  let
    time = now()
  var f = open(getEnv("HOME") & "/.config/i3blocks/batt.log", fmappend)
  defer: f.close()
  f.write($time & " [" & perc & "]\n")
  
proc main(): void =
  let
    perc = batt()

  if paramCount() != 1:
    echo perc & "%"
  elif paramStr(1) == "-i3":
    log(perc)
    echo "B: " & perc & "%"
    echo perc & "%"

    if perc >= 65.0:
      echo  "#72d5a3"
    elif perc < 65.0 and perc >= 45.0:
      echo "#f0dfaf"
    elif perc < 45.0 and perc > 25.0:
      echo "#dfaf8f"
    elif perc < 25.0:
      echo "#dca3a3"

main()
