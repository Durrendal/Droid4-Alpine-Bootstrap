import std/[strutils]

type
  Features = tuple
    displayOn: bool
    displayBlank: bool
    usbNet: bool
    usb: bool
    mxtts: bool
    omap2430: bool

