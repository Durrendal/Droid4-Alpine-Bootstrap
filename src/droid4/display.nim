import std/[strutils, osproc]
import util

proc setDisplayBrightness*(setting: string): void =
  let
    sys = "/sys/class/backlight/backlight"

  case setting:
    of "on":
      setBrightnessValue(sys, "brightness", 3)
    of "off":
      setBrightnessValue(sys, "brightness", 0)
    else:
      setBrightnessValue(sys, "brightness", setting.parseInt())

proc setDisplayMode*(mode: string): void =
  let
    xrMobile = "xrandr --output DSI-1 --auto --rotate right --output HDMI-1 --off"
    xrExternal = "xrandr --output HDMI-1 --auto --output DSI-1 --off"
    xrLapdock = """
xrandr --newmode "1366x768_60.00"  85.86 1368 1440 1584 1800  768 769 772 795 -Hsync +Vsync
xrandr --addmode HDMI-1 1366x768_60.00 
xrandr --output HDMI-1 --mode 1366x768_60.00 --output DSI-1 --off
"""
  case mode:
    of "mobile":
      discard execCmd(xrMobile)
    of "lapdock":
      discard execCmd(xrLapdock)
    of "external":
      discard execCmd(xrExternal)

