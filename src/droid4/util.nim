import std/[strutils]

proc getValue*(path: string): string =
  let
    f = open(path)
  return f.readLine()

proc setTriggerValue*(path: string, class: string, triggers: seq[string], trigger: string): void =
  let
    syspath = path & "/" & class & "/trigger"

  if triggers.find(trigger) != -1:
    let
      f = open(syspath, fmWrite)
    defer: f.close()
    f.write(trigger)
  else:
    echo trigger & " is not a valid trigger for " & class
    echo "Triggers: " & triggers

proc setBrightnessValue*(path: string, class: string, val: int): void =
  let
    maxb = getValue(path & "/" & "max_brightness")
    syspath = path & "/" & class
  if val <= parseInt(maxb) and val >= 0:
    let
      f = open(syspath, fmWrite)
    defer: f.close()
    f.write(val)
  else:
    echo intToStr(val) & " exceeds max value for " & class & ". Valid range 0-" & maxb
