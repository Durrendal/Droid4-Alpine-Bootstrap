import std/[os, strutils], illwill
import util, leds, display

const
  long = """
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum nulla ex, commodo sed accumsan eu, egestas id eros. Vestibulum commodo, erat id lobortis accumsan, turpis augue efficitur ex, non iaculis sapien neque in lectus. Mauris ultricies tellus ut enim euismod, vel convallis dui pretium. Etiam id sem lacus. Donec faucibus nec purus at posuere. In malesuada, arcu nec malesuada mollis, nisl libero cursus tortor, sit amet iaculis erat ligula sit amet ante. Pellentesque ac ultricies magna. Mauris molestie, est eget congue porta, velit ligula dapibus metus, nec vulputate ipsum dolor quis odio. Nulla nec lorem nisl.
"""

proc drawBuffer() =
  var
    tbuf = newTerminalBuffer(terminalWidth(), terminalHeight())

  tbuf.clear()
  tbuf.drawRect(0, 0, tbuf.width-1, tbuf.height-1)
  tbuf.write(2, 1, fgBlue, "Droid XT894 Dashboard")
  tbuf.setForegroundColor(fgWhite, true)
  tbuf.drawHorizLine(2, tbuf.width-3, 2, true)
  tbuf.setForegroundColor(fgWhite, true)
  tbuf.drawHorizLine(2, tbuf.width-3, tbuf.height-5, true)
  tbuf.write(2, tbuf.height-3, fgBlue, "[H]", fgWhite, "ealth/", fgBlue, "[D]", fgWhite, "isplay/", fgBlue, "[L]", fgWhite, "ed/", fgBlue, "[V]", fgWhite, "ibe/", fgBlue, "[W]", fgWhite, "wan/", fgBlue, "[P]", fgWhite, "ower")

  tbuf.display()

proc exitProc() {.noconv.} =
  illwillDeinit()
  showCursor()
  quit(0)

proc drawDashboard*() =
  illwillInit(fullscreen=true)
  setControlCHook(exitProc)
  hideCursor()
  
  drawBuffer()

  while true:
    var
      key = getKey()
    case key
    of Key.None:
      discard
    of Key.Escape, Key.Q:
      exitProc()
    of Key.H:
      drawBuffer()
    of Key.D:
      drawBuffer()
    of Key.L:
      drawBuffer()
    of Key.V:
      drawBuffer()
    of Key.W:
      drawBuffer()
    of Key.P:
      drawBuffer()
    else:
      exitProc()
      
    sleep(20)
