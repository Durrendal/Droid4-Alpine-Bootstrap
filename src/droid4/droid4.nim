import docopt
import leds, display, dashboard
#nim c -d:release droid4.nim

const doc = """
Usage:
  droid4 dash
  droid4 disp <setting>
         [on, off, #, mobile, lapdock, external]
  droid4 led <name> <setting>
         [keyboard, display, red, green, blue, shift, button]
         [on, off, #]
  droid4 trigger <name> <setting>
         [shift]
         [none, cpu, mmc1, battery-charging, battery-full]
  droid4 pm
"""

proc led(name: string, setting: string): void =
  setLed(name, setting)

proc disp(setting: string): void =
  case setting:
    of "mobile", "lapdock", "external":
      setDisplayMode(setting)
    of "on", "off":
      setDisplayBrightness(setting)
    else:
      setDisplayBrightness(setting)

proc trigger(name: string, setting: string): void =
  setLedTrigger(name, setting)

#proc pm(setting: string): void =

#proc vibe(ms: string): void =

proc dashboard(): void =
  drawDashboard()

proc main(): void =
  #configure()
  
  let
    args = docopt(doc, version = "0.1")

  if args["led"]:
    led($args["<name>"], $args["<setting>"])
  elif args["disp"]:
    disp($args["<setting>"])
  elif args["trigger"]:
    trigger($args["<name>"], $args["<setting>"])
  elif args["dash"]:
    dashboard()
  else:
    echo doc

when isMainModule:
  main()
