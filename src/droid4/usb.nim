import std/[strutils]
import util

proc probeUdcModules(): void =
  let
    probe = """
modprobe libcomposite
modprobe musb_hdrc
modprobe phy_cpcap_usb
modprobe omap2430
"""
    discard execCmd(probe)
    
proc usbSerial(smac: string, uaddr: string): void =
  let
    mac = "ip link set dev usb0 address de:ab:6a:7a:fc:0b"
    intf = "ip address add " + uaddr + ".2 dev usb0"
    route = "ip route add default " + uaddr + ".0/24 dev usb0 "
    
