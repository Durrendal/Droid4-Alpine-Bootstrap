import std/[strutils]
import util

type
  Led = tuple
    class: string
    defaultOn: int
    defaultOff: int

var
  keyboard: Led = (class: "lm3532::kbd_backlight", defaultOn: 80, defaultOff: 0)
  display: Led = (class: "lm3532::backlight", defaultOn: 100, defaultOff: 0)
  red: Led = (class: "status-led:red", defaultOn: 10, defaultOff: 0)
  green: Led = (class: "status-led:green", defaultOn: 10, defaultOff: 0)
  blue: Led = (class: "status-led:blue", defaultOn: 10, defaultOff: 0)
  shift: Led = (class: "shift-key-light", defaultOn: 1, defaultOff: 0)
  button: Led = (class: "button-backlight", defaultOn: 1, defaultOff: 0)

proc setLedTrigger*(name: string, trigger: string): void =
  let
    sys = "/sys/class/leds"
    triggers = @["none", "kbd-scrolllock", "kbd-numlock", "kbd-capslock", "kbd-kanalock", "kbd-shiftlock", "kbd-altgrlock", "kbd-ctrllock", "kbd-altlock", "kbd-shiftllock", "kbd-shiftrlock", "kbd-ctrlllock", "kbd-ctrlrlock", "cpu", "cpu0", "cpu1", "mmc3", "mmc4", "mmc0", "mmc1", "mmc2", "usb-online", "battery-charging-or-full", "battery-charging", "battery-full", "battery-charging-blink-full-solid"]
  var
    class = ""

  case name:
    of "shift":
      class = shift.class
    of "button":
      class = button.class
    of "red":
      class = red.class
    of "green":
      class = green.class
    of "blue":
      class = blue.class
    else:
      echo name & " is an invalid class"
      return

  setTriggerValue(sys, class, triggers, trigger)

proc setLed*(name: string, setting: string): void =
  let
    sys = "/sys/class/leds/"

  case name:
    of "keyboard":
      case setting:
        of "on":
          setBrightnessValue(sys & keyboard.class, "brightness", keyboard.defaultOn)
        of "off":
          setBrightnessValue(sys & keyboard.class, "brightness", keyboard.defaultOff)
        else:
          setBrightnessValue(sys & keyboard.class, "brightness", setting.parseInt())
    of "display":
      case setting:
        of "on":
          setBrightnessValue(sys & display.class, "brightness", display.defaultOn)
        of "off":
          setBrightnessValue(sys & display.class, "brightness", display.defaultOff)
        else:
          setBrightnessValue(sys & display.class, "brightness", setting.parseInt())
    of "red":
      case setting:
        of "on":
          setBrightnessValue(sys & red.class, "brightness", red.defaultOn)
        of "off":
          setBrightnessValue(sys & red.class, "brightness", red.defaultOff)
        else:
          setBrightnessValue(sys & red.class, "brightness", setting.parseInt())
    of "blue":
      case setting:
        of "on":
          setBrightnessValue(sys & blue.class, "brightness", blue.defaultOn)
        of "off":
          setBrightnessValue(sys & blue.class, "brightness", blue.defaultOff)
        else:
          setBrightnessValue(sys & blue.class, "brightness", setting.parseInt())
    of "green":
      case setting:
        of "on":
          setBrightnessValue(sys & green.class, "brightness", green.defaultOn)
        of "off":
          setBrightnessValue(sys & green.class, "brightness", green.defaultOff)
        else:
          setBrightnessValue(sys & green.class, "brightness", setting.parseInt())
    of "shift":
      case setting:
        of "on":
          setBrightnessValue(sys & shift.class, "brightness", shift.defaultOn)
        of "off":
          setBrightnessValue(sys & shift.class, "brightness", shift.defaultOff)
        else:
          setBrightnessValue(sys & shift.class, "brightness", setting.parseInt())
    of "button":
      case setting:
        of "on":
          setBrightnessValue(sys & button.class, "brightness", button.defaultOn)
        of "off":
          setBrightnessValue(sys & button.class, "brightness", button.defaultOff)
        else:
          setBrightnessValue(sys & button.class, "brightness", setting.parseInt())
