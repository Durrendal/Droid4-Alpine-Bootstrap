#!/bin/ash

lapdock() {
	xrandr --newmode "1366x768_60.00"  85.86 1368 1440 1584 1800  768 769 772 795 -Hsync +Vsync
	xrandr --addmode HDMI-1 1366x768_60.00 
	xrandr --output HDMI-1 --mode 1366x768_60.00 --output DSI-1 --off
}

mobile() {
	xrandr --output DSI-1 --auto --rotate right --output HDMI-1 --off
}

external() {
	xrandr --output HDMI-1 --mode 1920x1080 --output DSI-1 --off
}

if [ -z $1 ] || [ "$1" == "-h" ]; then
   printf "Usage: dscreen.sh [-m] [-l] [-e]
-m | xrandr mobile config
-l | xrandr lapdock config
-e | xrandr external config
" && exit 1
   else
       while getopts "mle" opt; do
		   case $opt in
			   m) mobile ;;
			   l) lapdock ;;
			   e) external ;;
		   esac
	   done
fi
