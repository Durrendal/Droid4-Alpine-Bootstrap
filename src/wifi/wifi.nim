import std/[os, osproc, math, strutils]
#nim c -d:release wifi.nim

#Expand proc & to concat float & strings
proc `&` (f: float, s:string): string = $f & s
proc `&` (s: string, f: float): string = s & $f

#Pull link value from proc file for given interface
proc getLink(wireless: string, intf: string): string =
  for line in lines(wireless):
    if line.contains(intf):
      let
        link = line.split(' ')
      return link[5].strip(chars = {'.'})

proc getWifiStr(intf: string): float =
  let
    link = getLink("/proc/net/wireless", intf).strip().parseInt
    perc = (link * 100 / 70)
  return round(perc)

proc getIntfAddr(intf: string): string =
  let
    stats = execCmdEx("ip addr show dev " & intf)
  for line in splitLines(stats.output):
    if line.contains("inet"):
      let
        address = line.split(' ')
      return address[5]

proc main(): void =
  let
    perc = getWifiStr("wlan0")
    address = getIntfAddr("wlan0")

  if paramCount() != 1:
    echo perc & "%"
  elif paramStr(1) == "-i3":
    echo "W: " & address & " " & perc & "%"
    echo perc & "%"

    if perc > 65.0:
      echo "#72d5a3"
    elif perc <= 65.0 and perc >= 45.0:
      echo "#f0dfaf"
    elif perc < 45.0 and perc >= 25.0:
      echo "#dfaf8f"
    elif perc < 25.0:
      echo "#dca3a3"

main()
