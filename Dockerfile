FROM debian:latest

COPY ["./scripts/build-kern.sh", "./scripts/build-mods.sh", "/usr/local/bin/"]

RUN apt-get update -y ;\
	DEBIAN_FRONTEND="noninteractive" apt-get updrade -y ;\
	DEBIAN_FRONTEND="noninteractive" apt-get install libncurses5-dev exuberant-ctags libssl-dev gcc-arm-linux-gnueabihf git tmux mg gcc make lzma bison kmod qemu-user-static flex bc sudo -y ;\
	mkdir /linux ;\
	mkdir modules ;\
	chmod +x /usr/local/bin/*.sh

VOLUME /linux
VOLUME /modules

CMD ["tmux"]
