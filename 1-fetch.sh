#!/bin/ash

docker run --rm --privileged multiarch/qemu-user-static:register --reset --credential yes

wget https://maedevu.maemo.org/images/droid4/VRZ_XT894_9.8.2O-72_VZW-18-8_CFC.xml.zip -P archives/stock_rom/droid4

unzip archives/stock_rom/droid4/VRZ_XT894_9.8.2O-72_VZW-18-8_CFC.xml.zip -d archives/stock_rom/droid4

rm archives/stock_rom/droid4/VRZ_XT894_9.8.2O-72_VZW-18-8_CFC.xml.zip

wget -P archives/stock_rom/droid4 -c  https://raw.githubusercontent.com/omerlle/droid4-bringup/alpine/boot/flash-droid-4-fw.sh

sed -i 's/fastboot=fastboot/fastboot="sudo fastboot"/' archives/stock_rom/droid4/flash-droid-4-fw.sh

git clone https://github.com/tmlind/droid4-kexecboot.git archives/kexecboot/droid4/

#git clone https://github.com/tmlind/linux linux/tmlind

cd linux/tmlind

git checkout -b local/v5.15 origin/droid4-pending-v5.15

cd ../..

wget -P archives/basic_rootfs/alpine/ -c http://dl-cdn.alpinelinux.org/alpine/edge/releases/armv7/alpine-minirootfs-edge-armv7.tar.gz

git clone https://github.com/omerlle/droid4-bringup.git archives/droid4_bringup

git -C archives/droid4_bringup/ checkout alpine

wget -c https://github.com/TI-ECS/bt-firmware/raw/master/TIInit_10.6.15.bts -P archives/firmware/ti-connectivity
echo "Dependencies fetched"
