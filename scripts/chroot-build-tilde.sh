#!/bin/ash
#Build a tilde server with your Droid4
#Idea derived from Netscape_Navigator

echo "Adding Admin Packages"
apk add htop iftop bmon findutils iputils util-linux pciutils usbutils coreutils binutils recutils tmux finger shadow ncdu ranger mtr rsync neofetch tree

echo "Adding editors"
apk add mg vim nano emacs mg micro sc

echo "Adding Languages"
apk add fennel lua lua5.2 lua5.3 lua5.4 luajit python3 gcc sbcl racket tinyscheme gforth retroforth go bash

echo "Adding Development Tools"
apk add luarocks py3-pip make strace git abuild cmake extra-cmake-modules ccache automake autoconf

echo "Adding Data Handling Tools"
apk add jq xsltproc curl unzip apsell aspell-en apsell-fr
#recutils

echo "Adding Supplemental Libs"
apk add openssl openssl-dev sqlite sqlite-dev linux-headers lua-dev lua5.2-dev lua5.3-dev lua5.4-dev libc-dev musl-dev racket-dev curl-dev

#nginx uses about 3mb of ram, lighttpd uses about 1mb
echo "Adding Web Hosting Packages"
apk add lighttpd

echo "Add Surfing Packages"
apk add links2 lynx w3m amfora

echo "Adding cli games"
apk add nethack zangband frotz

echo "Configure webserver"
cp /configs/tilde.conf /etc/lighttpd/lighttpd.conf

echo "Enable webserver"
rc-update add lighttpd default

echo "Create Skel"
mkdir /etc/skel/public_html
mkdir /etc/skel/scripts
cp /configs/tilde-userpage.html /etc/skel/public_html/index.html
cp /configs/tilde-profile /etc/skel/.profile
chmod -R 644 /etc/skel/*

mkdir /etc/skel/secrets
cp /configs/tilde-secrets /etc/skel/secrets/notice.txt
chmod -R 700 /etc/skel/secrets

echo "Disable phone centric configurations"
rc-update del droid4-modem-app

echo "Change hostname"
echo droidxt894 > /etc/hostname
sed -i 's/persistemo/droidxt894/' /etc/hosts

echo "Add tildebots group"
groupadd tildebots

echo "Create /tilde directory"
mkdir /tilde
chown root:tildebots /tilde
chmod 775 /tilde
