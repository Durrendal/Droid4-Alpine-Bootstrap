#!/bin/ash
username=$1

echo "Adding i3 packages"
apk add i3wm i3status i3lock i3blocks lightdm xterm lightdm-gtk-greeter font-iosevka-nerd ttf-liberation font-liberation-nerd ttf-dejavu ttf-freefont ttf-cantarell ttf-opensans

echo "Configure Xorg"
setup-xorg-base

echo "Build utilities"
go build /tmp/src/battery.go
cp /tmp/src/battery /usr/local/bin/

echo "Configuring i3"
mkdir -p /home/$username/.config/i3
mkdir /home/$username/.config/i3blocks
cp /configs/i3.config /home/$username/.config/i3/config
cp /configs/i3blocks/* /home/$username/.config/i3blocks/

echo "Configuring .xinitrc"
cp /configs/.xinitrc /home/$username/.xinitrc
sed -i 's/WMSTR/i3/' /home/$username/.xinitrx
chown $username:$username /home/$username/.xinitrc
