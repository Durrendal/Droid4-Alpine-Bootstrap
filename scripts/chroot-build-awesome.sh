#!/bin/ash
username=$1

echo "Adding awesome packages"
apk add awesome lightdm xterm lightdm-gtk-greeter arc arc-dark arc-darker arc-lighter font-iosevka-nerd papirus-icon-theme ttf-dejavu ttf-freefont ttf-cantarell ttf-opensans

echo "Configure Xorg"
setup-xorg-base

echo "Adding base awesome config to $username"
mkdir -p /home/$username/.config/awesome/
cp /etc/xdg/awesome/rc.lua /home/$username/.config/awesome/

echo "Configuring .xinitrc"
cp /config/.xinitrc /home/$username/.xinitrc
sed -i 's/WMSTR/awesome/' /home/$username/.xinitrc
chown $username:$username /home/$username/.xinitrc
