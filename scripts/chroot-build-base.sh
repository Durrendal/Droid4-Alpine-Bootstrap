#!/bin/ash
username=$1

echo "Setting Edge Repos"
printf "http://dl-cdn.alpinelinux.org/alpine/edge/main
http://dl-cdn.alpinelinux.org/alpine/edge/community
http://dl-cdn.alpinelinux.org/alpine/edge/testing\n" > /etc/apk/repositories

echo "Updating Base"
apk update
apk upgrade

echo "Adding patched Xorg Driver"
apk add xorg-server='21.1.3-r9999' /packages/xorg-server-21.1.3-r9999.apk --allow-untrusted

echo "Adding Base Tools"
apk add bash openrc udev elogind openssh python3 py3-pip alsa-utils util-linux rsync tmux feh linux-headers build-base python3-dev sqlite kbd libqmi emacs linux-firmware wpa_supplicant bluez wireless-tools alpine-conf sudo htop tmux networkmanager pciutils pciutils-libs hwids-pci hwids-pci mtdev iwd acpi mg shadow xrandr arandr iputils consolekit2 polkit syslog-ng polkit usbutils findutils lsof busybox-initscripts logrotate go

echo "Adding XF86 Drivers"
apk add xf86-video-omap xf86-video-fbdev xf86-video-vesa xf86-video-modesetting xf86-input-keyboard xf86-input-synaptics xf86-input-mouse dbus-x11 xf86-input-libinput xf86-input-evdev libinput-libs xorg-server xinit xinput

echo "Ensuring latest Python3 evdev"
pip3 install --upgrade pip
pip3 install evdev

echo "Adding droid4 Keyboard/Touchscreen configuration"
cp /configs/motorola-geometry /usr/share/X11/xkb/geometry/motorola
mkdir -p /usr/share/X11/xkb/symbols/motorola_vndr
cp /configs/droid4-xkb-symbol /usr/share/X11/xkb/symbols/motorola_vndr/droid4
mkdir -p /etc/X11/xorg.conf.d
cp /configs/40-input.conf /etc/X11/xorg.conf.d/40-input.conf
cp /configs/40-ts.conf /etc/X11/xorg.conf.d/40-ts.conf
cp /configs/keyboard /etc/default/keyboard
mkdir -p /etc/udev/hwdb.d
cp /configs/50-keyboard-modifiers.hwdb /etc/udev/hwdb.d/50-keyboard-modifiers.hwdb

echo "Configure polkit rules"
cp /configs/01-org.freedesktop.NetworkManager.settings.modify.system.rules /etc/polkit/rules.d
chmod 700 /etc/polkit/rules.d/01-org.freedesktop.NetworkManager.settings.modify.system.rules

echo "Configure sysctl for no ipv6 and power savings"
cp /configs/sysctl.conf /etc/sysctl.conf
chmod 644 /etc/sysctl.conf

echo "Configure sysfs to mitigate cpu deadlocks"
cp /configs/sysfs.conf /etc/sysfs.conf
chmod 644 /etc/sysfs.conf

echo "Add rc local scripts"
cp /configs/cpu_patch.start /etc/local.d/cpu_patch.start
chmod 755 /etc/local.d/*

echo "Adding Power/Droid groups"
addgroup power
addgroup droid

echo "Adding $username & setting groups"
adduser -D -s /bin/ash $username
printf "droidxt894\ndroidxt894\n" | passwd $username
usermod -a -G wheel,power,droid,users,plugdev,input,netdev,audio,video,tty $username

echo "Building $username home"
mkdir /home/$username/Org
mkdir /home/$username/Development
mkdir -p /home/$username/Documents/books
mkdir /home/$username/Downloads
mkdir /home/$username/Scripts
cp /configs/profile /home/$username/.profile
cp /configs/.Xresources /home/$username/.Xresources
touch /home/$username/.fa
chown -R $username:$username /home/$username

echo "Changing root passwd"
printf "droidxt894\ndroidxt894\n" | passwd

echo "Adding wallpaper"
mkdir /usr/share/wallpapers
cp /configs/senforma.png /usr/share/wallpapers/

echo "Enabling wheel"
sed -i 's/^# %wheel ALL=(ALL) ALL/%wheel ALL=(ALL) ALL/' /etc/sudoers

echo "Enabling passwordless power commands"
echo "%droid ALL=NOPASSWD: /usr/local/bin/droid4-leds, /usr/local/bin/droid4-modem, /usr/local/bin/droid4-pm, /usr/local/bin/droid4-vibrator, /usr/local/bin/disp" | tee -a /etc/sudoers

echo "Enabling passwordless droid interface"
echo "%power ALL=NOPASSWD: /sbin/poweroff, /sbin/reboot, /sbin/halt" | tee -a /etc/sudoers

echo "Setting hostname"
echo persistemo > /etc/hostname
echo "127.0.0.1 persistemo" >> /etc/hosts

echo "Setting lo"
#For some reason our display server hangs if lo is started automatically
#There are obvious problems with networking on this system in general, apologies.
printf "" > /etc/network/interfaces

echo "Setting NetworkManager.conf to managed"
printf "[main]
dhcp=internal
[ifupdown]
managed=true\n" > /etc/NetworkManager/NetworkManager.conf

echo "Blacklisting hci_uart to enable wlan0"
echo "#This enables wlan0" | tee -a /etc/modprobe.d/blacklist.conf
echo "blacklist hci_uart" | tee -a /etc/modprobe.d/blacklist.conf

echo "Blacklisting phy_mapphone_mdm6600 for better battery life"
echo "This disables USB, Audio, and WWAN! Unblacklist if you want these features!"
echo "#This disables USB, Audio, and WWAN. Unblacklist if desired" | tee -a /etc/modprobe.d/blacklist.conf
echo "blacklist phy_mapphone_mdm6600" | tee -a /etc/modprobe.d/blacklist.conf

echo "Setting up init"
rc-update add devfs sysinit
rc-update add networking sysinit
rc-update add dmesg sysinit

rc-update add sysfs boot
rc-update add sysfsconf boot
rc-update add local boot
rc-update add hwclock boot
rc-update add hostname boot
rc-update add bootmisc boot
rc-update add syslog-ng boot

rc-update add sshd default
rc-update add droid4-startup default
#rc-update add droid4-usb default #If this is enabled the ip route defaults to 10.0.0.x, effectively mucking up networking
rc-update add droid4-modem-app default
rc-update add droid4-buttons-handle default
rc-update add networkmanager default
#wpa supplicant appears to cause instability issues with the networking, especially on kernel 5.10.x
#rc-update add wpa_supplicant default
rc-update add cpufreqd default
rc-update add polkit default
rc-update add dbus default
rc-update add elogind default
#rc-update add chronyd default #I replaced my battery, and not the HW clock doesn't work, this NTP service seems decent as a stop gap until I can reflash, maybe it should be in the base
#rc-update add altalt default #Needs to be added, but Esper should make it to the testing repo first so it's easier

#unsure on these two, I've seen 190% CPU spikes triggered by udev in this setting. There's likely a bug, but removing these from sysinit makes the boot hang significantly. It's a trade off. I'll likely remove them from mine
#rc-update del udev sysinit
#rc-update del udev-trigger sysinit 

echo "Enabling vfat partition automount"
echo "/dev/mmcblk0p3 /root/user_data vfat defaults" >> /etc/fstab

echo "Enabling boot partition automount"
echo "/dev/mmcblk0p1 /boot vfat nofail 0 2" >> /etc/fstab
mkdir /boot

echo "Populating databases"
sqlite3 /root/.droid4/hardware.db < /usr/local/share/droid4/python3_packages/hardware/config/hardware_db.back
sqlite3 /tmp/modem.db < /usr/local/share/droid4/python3_packages/modem/config/modem_db.back

echo "Prepping config scripts"
chmod +x /configs/*.sh
