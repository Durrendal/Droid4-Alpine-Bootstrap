#!/bin/ash
mkdir -p archives/stock_rom/droid4
mkdir -p archives/kexecboot/droid4
mkdir -p linux/linux-stable
mkdir mnt
mkdir modules
mkdir -p archives/firmware/ti-connectivity
mkdir -p archives/basic_rootfs/alpine
mkdir -p rootfs/alpine/edge_armv7
echo "layout built"

echo "preparing kernel compilation container"
docker build -t kernel-compile .

echo "ensuring script permissions"
chmod +x *.sh
