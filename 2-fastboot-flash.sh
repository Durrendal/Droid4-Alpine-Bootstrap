#!/bin/ash

cd archives/stock_rom/droid4

.././flash-droid-4-fw.sh

cd ../../..

sudo fastboot flash mbm archives/stock_rom/droid4/VRZ_XT894_9.8.2O-72_VZW-18-8_CFC.xml/allow-mbmloader-flashing-mbm.bin

sudo fastboot reboot bootloader

sudo fastboot flash bpsw archives/kexecboot/droid4/current/droid4-kexecboot.img

sudo fastboot flash utags archives/kexecboot/droid4/utags-mmcblk1p13.bin

sudo fastboot flash mbm archives/stock_rom/droid4/VRZ_XT894_9.8.2O-72_VZW-18-8_CFC.xml/mbm.bin
echo "system flashed"
