#!/bin/ash
droid_rootfs="$(pwd)/rootfs/alpine/3.14.2_armv7"
mkdir ${droid_rootfs}

sudo tar -xzf archives/basic_rootfs/alpine/alpine-minirootfs-3.14.3-armv7.tar.gz -C ${droid_rootfs}/

sudo rsync -a --chown=root:root archives/droid4_bringup/overlay/ ${droid_rootfs}

sudo cp /etc/resolv.conf ${droid_rootfs}/etc/

sudo cp -a qemu-arm-static ${droid_rootfs}/usr/bin/
sudo cp scripts/chroot-*.sh ${droid_rootfs}/usr/local/bin/
sudo chmod +x ${droid_rootfs}/usr/local/bin/chroot-*.sh

sudo mkdir ${droid_rootfs}tmp//packages
sudo cp packages/*.apk ${droid_rootfs}/tmp/packages/

sudo mkdir ${droid_rootfs}/tmp/configs
sudo cp -r configs ${droid_rootfs}/tmp/

sudo mkdir ${droid_rootfs}/tmp/src
sudo cp -r src ${droid_rootfs}/tmp/

sudo mount -t proc /proc ${droid_rootfs}/proc

sudo mount -o bind /dev ${droid_rootfs}/dev
echo "rootfs built, please chroot"
